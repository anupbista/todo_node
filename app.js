var express = require('express');
var todoController = require('./controller/todo-controller');

var app = express();

// set up templating engine
app.set('view engine','ejs');

// static files
app.use(express.static('./public'));

// fire controller
todoController(app);

//listen to port
app.listen(3000);
console.log('The Server is running in port 3000');