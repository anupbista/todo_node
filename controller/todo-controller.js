var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false });
var mongoose = require('mongoose');

// connect to database
mongoose.connect('mongodb://localhost/todo_node_db');
mongoose.connection.once('open', function(){
    console.log("Connectgion has been established to Mongodb");
}).once('error',function(error){
    console.log("Connection Erorr:"+error);
});
// Create a Schema

var todoSchema = new mongoose.Schema({
    item:String
});

var Todo = mongoose.model('Todo',todoSchema);

module.exports = function(app){
    app.get('/todo',function(req,res){
        // get data from mongodb on pass it to view
        Todo.find({},function(error, data){
            if(error) throw error;
            res.render('todo', {todos:data});
        });
    });

    app.post('/todo', urlencodedParser ,function(req,res){
        // get data from view and add it to mongodb
        var newTodo = Todo(req.body).save(function(err, data){
            if (err) throw err;
            res.json(data);
        });        
    });

    app.delete('/todo/:item',function(req,res){
        // delete the requested item from mongodb
        Todo.find({item: req.params.item.replace(/\-/g, "")}).remove(function(err, data){
            if (err) throw err;
            res.json(data);
        });
    });
};