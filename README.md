# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* A Todo App on Nodejs (Express Server) and Mongodb Database
* Version: 1.0.0

### How do I get set up? ###

* Install:
	-Nodejs
	-Mongodb
* Configuration:
	-Start Mongodb:  mongo --host 127.0.0.1:27017
	-(Optional) Install Nodemon(for autoserver restart on changes)
		-Run nodemon on terminal
	-Run app.js on terminal (if nodemon not installed)
	-App will run on localserver
* Dependencies: In package.json file
* Deployment: Not Deployed

### Who do I talk to? ###
* Anup Bista (http://www.anupbista.com.np)